## How to build the project:
Run CMakeRegenerate.bat from the script folder with the path to the built Chrono "cmake" folder as the first argument. Set the C++ version to C++17 in order to make the std::clamp function work.
# Members:
### Zota Iosif
### Stoica Marius
### Rapan Nicolae Cristian
### Ciocan Marius
### Basescu Alexandru-Gabriel