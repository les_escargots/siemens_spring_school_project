#include "Environment/ChronoApp.h"
#include "Environment/Exporter.h"
#include <thread>
#include <chrono>

ChronoApp::ChronoApp() : m_application(&m_system, L"les_escargatos Irrlicht FEM visualization", core::dimension2d<u32>(800, 600), false, true)
{
	PrepareAppForStart();
}

ChronoApp& ChronoApp::GetInstance()
{
	static ChronoApp instance;
	return instance;
}

void ChronoApp::SetupScene()
{
	/* Adds default assets to the scene*/
	m_application.AddTypicalLogo();
	m_application.AddTypicalSky();
	m_application.AddTypicalLights();
	m_application.AddTypicalCamera(core::vector3df(0, (f32)0.6, -1));
}

void ChronoApp::CreateObjects()
{
	/* Creates a wall and adds it to the system */
	auto wall = std::make_shared<chrono::ChBodyEasyBox>(1, 1, 0.1, 1);
	wall->SetBodyFixed(true);
	m_system.Add(wall);

	/* Creates a beam mesh */
	auto beamMesh = std::make_shared<ChMesh>();
	beamMesh->SetName("beamMesh");

	/* Creates a plastic material for the beam mesh */
	auto plasticMaterial = std::make_shared<ChContinuumPlasticVonMises>();

	/* Generates the beam */
	BeamGenerator::GenerateBeam("beam", 0.2, 0.7, 1, 9);
	try {
		ChMeshFileLoader::FromTetGenFile(beamMesh, "beam.node", "beam.ele", plasticMaterial);
	}
	catch (ChException myerr) {
		GetLog() << myerr.what();
		return;
	}

	/* Rotates the beam mesh */
	RotateBeamMesh(beamMesh);

	/* Creates Constraints between nodes and wall */
	CreateConstraintsNodesWall(beamMesh, wall);

	/* Loads must be added to "load containers" */
	auto loadContainer = std::make_shared<ChLoadContainer>();

	/* Load containers must be added to the system */
	m_system.Add(loadContainer);

	/* Gets the node # 90 && 92  */
	auto nodeNinety = std::dynamic_pointer_cast<ChNodeFEAxyz>(beamMesh->GetNode(90));
	auto nodeNinetyTwo = std::dynamic_pointer_cast<ChNodeFEAxyz>(beamMesh->GetNode(92));

	/* Applies the load to node #92 */
	//  AddLoadToNode(loadContainer, nodeNinetyTwo);

	/* Adds assets to the beam mesh */
	BeamMeshSurfaceMaterialConfiguration(beamMesh);

	/* Creates the cable and the box*/
	AttachCableWithBox(nodeNinety, nodeNinetyTwo);

	/* Adds the beam meash to the system */
	m_system.Add(beamMesh);
}

void ChronoApp::RotateBeamMesh(const std::shared_ptr<ChMesh> & beamMesh) const
{
	ChCoordsys<> cRotate(VNULL, Q_from_AngAxis(-90 * CH_C_DEG_TO_RAD, VECT_X)); cRotate;
	ChMatrix33<> m(cRotate.rot);
	for (auto node : beamMesh->GetNodes()) {
		auto chNode = std::dynamic_pointer_cast<ChNodeFEAxyz>(node);
		if (chNode != nullptr) {
			chNode->SetPos(m * chNode->GetPos() + ChVector<>(-0.1, -0.1, 0.05));
		}
	}
}

void ChronoApp::CreateConstraintsNodesWall(const std::shared_ptr<ChMesh> & beamMesh, const std::shared_ptr<ChBodyEasyBox> & wall)
{
	for (unsigned int inode = 0; inode < beamMesh->GetNnodes(); ++inode)
	{
		if (auto mnode = std::dynamic_pointer_cast<ChNodeFEAxyz>(beamMesh->GetNode(inode)))
		{
			if (mnode->GetPos().z() > -0.15) {
				auto constraint = std::make_shared<ChLinkPointFrame>();
				constraint->Initialize(mnode, wall);
				m_system.Add(constraint);

				auto mboxfloor = std::make_shared<ChBoxShape>();
				mboxfloor->GetBoxGeometry().Size = ChVector<>(0.005);
				constraint->AddAsset(mboxfloor);
			}
		}
	}
}

void ChronoApp::AddLoadToNode(const std::shared_ptr<ChLoadContainer> & loadContainer, const std::shared_ptr<ChNodeFEAxyz> & feaNode) const
{
	if (feaNode == nullptr)
		return;
	std::shared_ptr<ChLoad<LoaderStiffLoadNode>> mloadstiff(new ChLoad<LoaderStiffLoadNode>(feaNode));
	loadContainer->Add(mloadstiff);
}

void ChronoApp::BeamMeshSurfaceMaterialConfiguration(const std::shared_ptr<ChMesh> & beamMesh) const
{
	auto contactMaterial = std::make_shared<ChMaterialSurfaceSMC>();
	contactMaterial->SetYoungModulus(6e4);
	contactMaterial->SetFriction(0.1f);
	contactMaterial->SetRestitution(0.05f);

	auto contactSurface = std::make_shared<ChContactSurfaceMesh>();
	beamMesh->AddContactSurface(contactSurface);
	contactSurface->AddFacesFromBoundary();
	contactSurface->SetMaterialSurface(contactMaterial);

	auto visualizeStrain = std::make_shared<ChVisualizationFEAmesh>(*(beamMesh.get()));
	visualizeStrain->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_ELEM_STRAIN_VONMISES);
	visualizeStrain->SetColorscaleMinMax(0.0, 1);
	visualizeStrain->SetShrinkElements(true, 0.85);
	visualizeStrain->SetSmoothFaces(true);
	beamMesh->AddAsset(visualizeStrain);

	auto visualizeC = std::make_shared<ChVisualizationFEAmesh>(*(beamMesh.get()));
	visualizeC->SetFEMglyphType(ChVisualizationFEAmesh::E_GLYPH_NODE_DOT_POS);
	visualizeC->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NONE);
	visualizeC->SetSymbolsThickness(0.006);
	beamMesh->AddAsset(visualizeC);
}

void ChronoApp::ApplicationSetup()
{
	m_application.AssetBindAll();

	// ==IMPORTANT!== Use this function for 'converting' into Irrlicht meshes the assets
	// that you added to the bodies into 3D shapes, they can be visualized by Irrlicht!

	m_application.AssetUpdateAll();

	// Mark completion of system construction
	m_system.SetupInitial();

	//
	// THE SOFT-REAL-TIME CYCLE
	//

	m_system.SetTimestepperType(chrono::ChTimestepper::Type::EULER_IMPLICIT_LINEARIZED);

	m_system.SetSolverType(ChSolver::Type::MINRES);
	m_system.SetSolverWarmStarting(true);
	m_system.SetMaxItersSolverSpeed(40);
	m_system.SetTolForce(1e-10);

	m_application.SetTimestep(0.001);
}

void ChronoApp::AttachCableWithBox(const std::shared_ptr<ChNodeFEAxyz> & firstNode, const std::shared_ptr<ChNodeFEAxyz> & secondNode)
{
	/* Intialises the cable beam meshes */
	auto leftCableMesh = std::make_shared<ChMesh>();
	auto rightCableMesh = std::make_shared<ChMesh>();

	/* Sets the cable */
	auto cableSection = std::make_shared<ChBeamSectionCable>();
	cableSection->SetDiameter(0.015);
	cableSection->SetYoungModulus(0.01e9);
	cableSection->SetBeamRaleyghDamping(0.000);

	ChBuilderBeamANCF leftCableBuilder;
	ChBuilderBeamANCF rightCableBuilder;

	/* Builds the left cable */
	leftCableBuilder.BuildBeam(leftCableMesh,
		cableSection,
		10,
		firstNode->GetPos(),
		firstNode->GetPos() + ChVector<>(0, -0.7, 0)
	);

	/* Builds the right cable */
	rightCableBuilder.BuildBeam(rightCableMesh,
		cableSection,
		10,
		secondNode->GetPos(),
		firstNode->GetPos() + ChVector<>(0, -0.7, 0)
	);

	/* Creates a constraint between the cables and the default beam */
	auto cableNodeConstraint = std::make_shared<ChLinkPointPoint>();
	cableNodeConstraint->Initialize(leftCableBuilder.GetLastBeamNodes().front(), firstNode);
	m_system.Add(cableNodeConstraint);

	auto cableNodeConstraintTwo = std::make_shared<ChLinkPointPoint>();
	cableNodeConstraintTwo->Initialize(rightCableBuilder.GetLastBeamNodes().front(), secondNode);
	m_system.Add(cableNodeConstraintTwo);

	/* Creates a box */
	auto aBox = std::make_shared<ChBodyEasyBox>(0.1, 0.1, 0.1, 200);
	aBox->SetPos(rightCableBuilder.GetLastBeamNodes().back()->GetPos() + ChVector<>(0.05, 0, 0));
	m_system.Add(aBox);

	aBox->SetMass(1500);

	/* Creates a constraint between the box and the cables */
	auto constraintPos = std::make_shared<ChLinkPointFrame>();
	constraintPos->Initialize(leftCableBuilder.GetLastBeamNodes().back(), aBox);
	m_system.Add(constraintPos);

	auto constrainPosTwo = std::make_shared<ChLinkPointFrame>();
	constrainPosTwo->Initialize(rightCableBuilder.GetLastBeamNodes().back(), aBox);
	m_system.Add(constrainPosTwo);

	/* Adds the default cable visualization assets */
	auto addDefaultCableAsset = [&](auto & cableMesh)
	{

		auto cableVisualizeA = std::make_shared<ChVisualizationFEAmesh>(*(cableMesh.get()));
		cableVisualizeA->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_ELEM_BEAM_MZ);
		cableVisualizeA->SetColorscaleMinMax(-0.4, 0.4);
		cableVisualizeA->SetSmoothFaces(true);
		cableVisualizeA->SetWireframe(false);

		auto cableVisualizeB = std::make_shared<ChVisualizationFEAmesh>(*(cableMesh.get()));
		cableVisualizeB->SetFEMglyphType(ChVisualizationFEAmesh::E_GLYPH_NODE_DOT_POS);  // E_GLYPH_NODE_CSYS
		cableVisualizeB->SetFEMdataType(ChVisualizationFEAmesh::E_PLOT_NONE);
		cableVisualizeB->SetSymbolsThickness(0.006);
		cableVisualizeB->SetSymbolsScale(0.01);
		cableVisualizeB->SetZbufferHide(false);

		cableMesh->AddAsset(cableVisualizeA);
		cableMesh->AddAsset(cableVisualizeB);
	};

	addDefaultCableAsset(leftCableMesh);
	addDefaultCableAsset(rightCableMesh);

	/* Adds the cable */
	m_system.Add(leftCableMesh);
	m_system.Add(rightCableMesh);
}

// Used only by getLeastMostStrainElements
bool operator<(const ChStrainTensor<>& lhs, const ChStrainTensor<>& rhs) {
	return lhs.GetEquivalentVonMises() < rhs.GetEquivalentVonMises();
}

bool operator>(const ChStrainTensor<>& lhs, const ChStrainTensor<>& rhs) {
	return rhs < lhs;
}

tuple<ElementTetra_ptr, ElementTetra_ptr> ChronoApp::GetLeastMostStrainElements(const std::shared_ptr<ChMesh>& beamMesh) {
	ElementTetra_ptr least, most;

	auto elements = beamMesh->GetElements();
	auto itr = elements.begin();

	least = most = std::dynamic_pointer_cast<ChElementTetra_4>(*itr);
	while (++itr != elements.end()) {
		auto elemTetra_4 = std::dynamic_pointer_cast<ChElementTetra_4>(*itr);

		if (elemTetra_4->GetStrain() > most->GetStrain()) {
			most = elemTetra_4;
		}
		else if (elemTetra_4->GetStrain() < least->GetStrain()) {
			least = elemTetra_4;
		}
	}

	return { least , most };
}

void ChronoApp::StartMainLoop()
{
// Constant Logger - We do not need it yet
#if 0
	std::thread constantLog([this] {
		auto beamMesh = m_system.SearchMesh("beamMesh");

		while (m_application.GetDevice()->run()) {
			std::this_thread::sleep_for(std::chrono::seconds(2));
			// check again
			if (!m_application.GetDevice()->run()) break;

			auto leastMost = GetLeastMostStrainElements(beamMesh);
			auto least = std::get<0>(leastMost);
			auto most = std::get<1>(leastMost);
			GetLog() << "Most stressed element: " << most->GetStrain().GetEquivalentVonMises() << "\n";
			GetLog() << "Least stressed element: " << least->GetStrain().GetEquivalentVonMises() << "\n\n";
		}
		});
#endif // 0

	while (m_application.GetDevice()->run())
	{
		bool modified = 0;
		if (m_application.GetSystem()->GetChTime() == 0.20000000000000015)
		{
			auto beamMesh = m_system.SearchMesh("beamMesh");

			auto leastMost = GetLeastMostStrainElements(beamMesh);
			auto least = std::get<0>(leastMost);
			auto most = std::get<1>(leastMost);

			GetLog() << "Most stressed element: " << most->GetStrain().GetEquivalentVonMises() << "\n";
			GetLog() << "Least stressed element: " << least->GetStrain().GetEquivalentVonMises() << "\n\n";

			// Exporting the beamMesh before we delete a tetrahedron
			Exporter::writeMesh(beamMesh, "beamBefore.mesh");
			Exporter::writeFrame(beamMesh, "beamBefore.vtk", "beamBefore.mesh");

#if 0
			std::vector<int> leastNodesIndex;
			for (int i = 0; i < most->GetNnodes(); ++i)
			{
				leastNodesIndex.push_back(most->GetNodeN(i)->GetIndex());
			}
			std::string myTetra = std::to_string(leastNodesIndex[0]) + " " + std::to_string(leastNodesIndex[2]) + " " + std::to_string(leastNodesIndex[1]) + " " + std::to_string(leastNodesIndex[3]);
			// We need to remove this tetraHedron - Unimplemented yet
			std::cout << myTetra;
#endif // 0


			modified = 1;
			// Exporting the beamMesh after we deleted a tetrahedron
			Exporter::writeMesh(beamMesh, "beamAfter.mesh");
			Exporter::writeFrame(beamMesh, "beamAfter.vtk", "beamAfter.mesh");
			std::cout << "I am extracting the information...";
		}

		m_application.BeginScene();

		m_application.DrawAll();

		m_application.DoStep();

		m_application.EndScene();

		if (modified == 1)
		{
			m_application.SetPaused(true);
		}
	}
}

void ChronoApp::PrepareAppForStart()
{
	/* Sets the Chrono Data Path */
	SetChronoDataPath(CHRONO_DATA_DIR);

	/* Logs the message */
	GetLog() << "Copyright (c) 2017 projectchrono.org\nChrono version: " << CHRONO_VERSION << "\n\n";

	/* Sets up the default scene */
	SetupScene();

	/* Creates the objects */
	CreateObjects();

	/* Application setup */
	ApplicationSetup();
}


