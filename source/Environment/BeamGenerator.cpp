#include <Environment/BeamGenerator.h>

uint32_t BeamGenerator::GetNumberOfNodesPerStoryRow(uint32_t widthSplits) 
{
	return (2 + widthSplits);
}

uint32_t BeamGenerator::GetNumberOfNodesPerStory(uint32_t widthSplits) 
{
	return (2 + widthSplits) * (2 + widthSplits);
}

uint32_t BeamGenerator::GetNumberOfNodesPerWall(uint32_t widthSplits, uint32_t heightSplits) 
{
	return (2 + widthSplits) * (2 + heightSplits);
}

uint32_t BeamGenerator::GetNumberOfNodes(uint32_t widthSplits, uint32_t heightSplits)
{
	return (2 + widthSplits) * (2 + heightSplits) * (2 + widthSplits);
}

uint32_t BeamGenerator::GetNumberOfTetrahedrons(uint32_t widthSplits, uint32_t heightSplits) 
{
	return (1 + widthSplits) * (1 + heightSplits) * (1 + widthSplits) * 6;
}

uint32_t BeamGenerator::GetNumberOfCubesPerStoryRow(uint32_t widthSplits) 
{
	return (1 + widthSplits);
}

uint32_t BeamGenerator::GetNumberOfCubesPerStory(uint32_t widthSplits)
{
	return (1 + widthSplits) * (1 + widthSplits);
}

uint32_t BeamGenerator::GetNumberOfCubes(uint32_t widthSplits, uint32_t heightSplits) 
{
	return (1 + widthSplits) * (1 + heightSplits) * (1 + widthSplits);
}

std::array<uint32_t, 8> BeamGenerator::GetCubeNodeIndices(uint32_t cubeIndex, uint32_t widthSplits, uint32_t heightSplits) 
{
	uint32_t nodesPerStory = GetNumberOfNodesPerStory(widthSplits), nodesPerStoryRow = GetNumberOfNodesPerStoryRow(widthSplits);
	uint32_t numberOfCubes = GetNumberOfCubes(widthSplits, heightSplits), cubesPerStory = GetNumberOfCubesPerStory(widthSplits);
	uint32_t cubesPerStoryRow = GetNumberOfCubesPerStoryRow(widthSplits);
	uint32_t xOfCube = cubeIndex % cubesPerStoryRow, zOfCube = cubeIndex % cubesPerStory / cubesPerStoryRow;
	uint32_t nodesOffsetFromBase = cubeIndex / cubesPerStory * nodesPerStory + zOfCube * nodesPerStoryRow;
	return std::array<uint32_t, 8>
	{
		nodesOffsetFromBase + xOfCube,
			nodesOffsetFromBase + xOfCube + 1,
			nodesOffsetFromBase + xOfCube + 1 + nodesPerStoryRow,
			nodesOffsetFromBase + xOfCube + nodesPerStoryRow,
			nodesOffsetFromBase + xOfCube + nodesPerStory,
			nodesOffsetFromBase + xOfCube + 1 + nodesPerStory,
			nodesOffsetFromBase + xOfCube + 1 + nodesPerStoryRow + nodesPerStory,
			nodesOffsetFromBase + xOfCube + nodesPerStoryRow + nodesPerStory
	};
}

void BeamGenerator::PrintBeamNodes(std::ofstream& outputFile, double width, double height, uint32_t widthSplits, uint32_t heightSplits) 
{
	uint32_t nodesPerStory = GetNumberOfNodesPerStory(widthSplits), nodesPerStoryRow = GetNumberOfNodesPerStoryRow(widthSplits);
	uint32_t numberOfNodes = GetNumberOfNodes(widthSplits, heightSplits);
	double widthStep = width / (widthSplits + 1), heightStep = height / (heightSplits + 1);
	outputFile << numberOfNodes << " 3 0 0\n";
	for (uint32_t nodeIndex = 0; nodeIndex < numberOfNodes; ++nodeIndex) 
	{
		outputFile << nodeIndex + 1 << " "; // Chrono counts things starting from 1
		outputFile << widthStep * (nodeIndex % nodesPerStoryRow) << " ";
		outputFile << heightStep * (nodeIndex / nodesPerStory) << " ";
		outputFile << widthStep * (nodeIndex % nodesPerStory / nodesPerStoryRow) << "\n";
	}
}

void BeamGenerator::PrintBeamTetrahedronsIndices(std::ofstream& outputFile, uint32_t widthSplits, uint32_t heightSplits) 
{
	uint32_t nodesPerStory = GetNumberOfNodesPerStory(widthSplits), nodesPerStoryRow = GetNumberOfNodesPerStoryRow(widthSplits);
	uint32_t numberOfNodes = GetNumberOfNodes(widthSplits, heightSplits), numberOfTetrahedrons = GetNumberOfTetrahedrons(widthSplits, heightSplits);
	uint32_t numberOfCubes = GetNumberOfCubes(widthSplits, heightSplits);
	outputFile << numberOfTetrahedrons << " 4 0\n";
	for (uint32_t cubeIndex = 0; cubeIndex < numberOfCubes; cubeIndex++) 
	{
		auto cubeIndices = GetCubeNodeIndices(cubeIndex, widthSplits, heightSplits);
		outputFile << cubeIndex * 6 + 0 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[1] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[0] + 1 << "\n";
		outputFile << cubeIndex * 6 + 1 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[5] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[1] + 1 << "\n";
		outputFile << cubeIndex * 6 + 2 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[6] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[5] + 1 << "\n";
		outputFile << cubeIndex * 6 + 3 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[7] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[6] + 1 << "\n";
		outputFile << cubeIndex * 6 + 4 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[3] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[7] + 1 << "\n";
		outputFile << cubeIndex * 6 + 5 + 1 << " ";
		outputFile << cubeIndices[4] + 1 << " " << cubeIndices[0] + 1 << " " << cubeIndices[2] + 1 << " " << cubeIndices[3] + 1 << "\n";
	}
}

void BeamGenerator::GenerateBeam(const std::string& fileName, double width, double height, uint32_t widthSplits, uint32_t heightSplits)
{
	std::ofstream nodeOut(fileName + ".node"), eleOut(fileName + ".ele");
	PrintBeamNodes(nodeOut, width, height, widthSplits, heightSplits);
	PrintBeamTetrahedronsIndices(eleOut, widthSplits, heightSplits);
}
