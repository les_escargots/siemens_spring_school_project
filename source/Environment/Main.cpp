#include <Environment/ChronoApp.h>

int main(int argc, char* argv[]) {

	auto& app = ChronoApp::GetInstance();
	app.StartMainLoop();

	return EXIT_SUCCESS;
}
