#include <Environment/LoaderStiffLoadNode.h>

LoaderStiffLoadNode::LoaderStiffLoadNode(std::shared_ptr<ChLoadableUVW> mloadable) :
	ChLoaderUVWatomic(mloadable, 0, 0, 0) {};
// Compute F=F(u)
// This is the function that you have to implement. It should return the F load at U,V,W.
// For ChNodeFEAxyz, loads are expected as 3-rows vectors, containing F absolute force.
// As this is a stiff force field, dependency from state_x and state_y must be considered.
void LoaderStiffLoadNode::ComputeF(const double U, const double V, const double W, ChVectorDynamic<>& F, ChVectorDynamic<>* state_x, ChVectorDynamic<>* state_w) 
{
	ChVector<> node_pos;
	ChVector<> node_vel;
	if (state_x) 
	{
		node_pos = state_x->ClipVector(0, 0);
		node_vel = state_w->ClipVector(0, 0);
	}
	else
	{
		node_pos = std::dynamic_pointer_cast<ChNodeFEAxyz>(loadable)->GetPos();
		node_vel = std::dynamic_pointer_cast<ChNodeFEAxyz>(loadable)->GetPos_dt();
	}
	// Just implement a simple force+spring+damper in xy plane,
	// for spring&damper connected to absolute reference:
	double Ky = -600;
	double Dy = 0.9;
	double y_offset = 15;
	double y_force = 100;
	// Store the computed generalized forces in this->load_Q, same x,y,z order as in state_w
	F(0) = 12000;															  // Fx component of force
	F(1) = y_force - Ky * (node_pos.y() - y_offset) - Dy * node_vel.y();  // Fy component of force
	F(2) = 0;                                                             // Fz component of force
}
// Remember to set this as stiff, to enable the jacobians
bool LoaderStiffLoadNode::IsStiff() { return true; }