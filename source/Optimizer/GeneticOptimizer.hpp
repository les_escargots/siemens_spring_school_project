#include "Optimizer/GeneticOptimizer.h"
#include <limits>

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::SortPopulation()
{
	if (!m_functionToOptimize)
	{
		throw std::exception("Didn't set the function that needs to be optimized.");
	}
	std::sort(m_population.m_population.begin(), m_population.m_population.end(), [&](const Individual<nGenes>& first, const Individual<nGenes>& second)
	{
		return m_functionToOptimize(first.GetChromosomes()) < m_functionToOptimize(second.GetChromosomes());
	});
}

template<size_t nGenes>
inline auto GeneticOptimizer<nGenes>::GetSelectionProbabilities() -> std::vector<double>
{
	SortPopulation();

	double eSum = 0.0;
	double const e = 2.71828182845904523536;
	uint32_t populationSize = m_population.m_population.size();
	for (const Individual<nGenes>& individual : m_population.m_population)
	{
		double result = m_functionToOptimize(individual.GetChromosomes());
		eSum += std::pow(e, std::clamp(result, -30.0, 30.0));
	}
	std::vector<double> probabilities(populationSize);
	for (int iIndividual = 0; iIndividual < populationSize; ++iIndividual)
	{
		double result = m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes());
		probabilities[iIndividual] = std::pow(e, std::clamp(result, -30.0, 30.0)) / eSum;
	}
	if (m_howToOptimize == false)
	{
		for (int iIndividual = 0; iIndividual < populationSize / 2; ++iIndividual)
		{
			std::swap(probabilities[iIndividual], probabilities[populationSize - 1 - iIndividual]);
		}
	}
	return probabilities;
}

template<size_t nGenes>
inline auto GeneticOptimizer<nGenes>::GetCummulativeProbabilities() -> std::vector<double>
{
	auto probabilities = GetSelectionProbabilities();
	std::vector<double> cummulativeProbabilities(probabilities.size(), 0.0);
	for (int iIndividual = 0; iIndividual < probabilities.size(); ++iIndividual)
	{
		for (int i = 0; i <= iIndividual; i++)
		{
			cummulativeProbabilities[iIndividual] += probabilities[i];
		}
	}
	return cummulativeProbabilities;
}

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::ReselectPopulation()
{
	auto cummulativeProbabilities = GetCummulativeProbabilities();
	uint32_t populationSize = m_population.m_population.size();

	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<> probDist{ 0, 1 };

	Population<nGenes> newPopulation;
	newPopulation.m_genesToMutate = m_population.m_genesToMutate;
	for (int iIndividual = 0; iIndividual < populationSize; ++iIndividual) {
		double selectionNumber = probDist(randEngine);
		for (int iSelection = 0; iSelection < populationSize; ++iSelection) {
			if (iSelection == 0 && selectionNumber <= cummulativeProbabilities[iSelection]) {
				newPopulation.m_population.push_back(m_population.m_population[iSelection]);
				break;
			}
			else if (iSelection != 0 && selectionNumber > cummulativeProbabilities[iSelection - 1] && selectionNumber <= cummulativeProbabilities[iSelection]) {
				newPopulation.m_population.push_back(m_population.m_population[iSelection]);
				break;
			}
		}
	}
	m_population = newPopulation;
}

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::SetPopulation(const Population<nGenes>& population)
{
	m_population = population;
}

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::SetFunctionToOptimize(std::function<double(const std::vector<std::bitset<nGenes>>&)> functionToOptimize)
{
	m_functionToOptimize = functionToOptimize;
}

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::SetHowToOptimize(bool howToOptimize)
{
	m_howToOptimize = howToOptimize;
}

template<size_t nGenes>
inline void GeneticOptimizer<nGenes>::Optimize(const std::string& fileName, uint32_t numberOfGenerations, bool verbose)
{
	if (!m_functionToOptimize)
	{
		throw std::exception("Didn't set the function that needs to be optimized.");
	}
	std::ofstream fout(fileName);
	Individual<nGenes> fittestIndividual;
	double fittestValue = (m_howToOptimize) ? std::numeric_limits<double>::min() : std::numeric_limits<double>::max();
	fout << "#start\n";
	for (uint32_t iGeneration = 0; iGeneration < numberOfGenerations; ++iGeneration)
	{
		fout << "\n#generationNo: " << iGeneration << "\n";
		SortPopulation();
		if (verbose == true)
		{
			std::cout << m_population << "\n\n";
		}
		for (uint32_t iIndividual = 0; iIndividual < m_population.m_population.size(); ++iIndividual)
		{
			fout << "Chromosome ";
			for (const auto& chromosome : m_population.m_population[iIndividual].GetChromosomes())
			{
				fout << chromosome.to_string() << " ";
			}
			fout << "\nValue " << m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes()) << "\n";
			if (m_howToOptimize == false)
			{
				if (m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes()) < fittestValue)
				{
					fittestValue = m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes());
					fittestIndividual = m_population.m_population[iIndividual];
				}
			}
			else
			{
				if (m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes()) > fittestValue)
				{
					fittestValue = m_functionToOptimize(m_population.m_population[iIndividual].GetChromosomes());
					fittestIndividual = m_population.m_population[iIndividual];
				}
			}
		}
		ReselectPopulation();
		m_population = m_population.GenerateChildren();
		m_population.Mutate();
	}
	fout << "\n#end\n";
	fout << "Maximum/Minimum chromosome ";
	for (const auto& chromosome : fittestIndividual.GetChromosomes())
	{
		fout << chromosome.to_string() << " ";
	}
	fout << "\nMaximum/Minimum value " << m_functionToOptimize(fittestIndividual.GetChromosomes());
	fout.close();
}
