#include <iostream>
#include "Optimizer/Individual.h"
#include "Optimizer/Population.h"
#include "Optimizer/GeneticOptimizer.h"
#include "Optimizer/BitsToValueConverter.h"

double f1(const std::vector<std::bitset<32>>& vars) // use chromosomes in the function to optimize to make the optimizer to use it
{
	// and convert to their values in the function
	const float x = BitsToValueConverter<32>(vars[0]).valueReal32;
	return std::pow(x, 2) + 6 * x + 1;
}

double f2(const std::vector<std::bitset<32>>& vars)
{
	const double pi = 3.141592;
	const float x = BitsToValueConverter<32>(vars[0]).valueReal32, y = BitsToValueConverter<32>(vars[1]).valueReal32;
	return std::sin(pi * 10 * x + 10 / (1 + std::pow(y, 2))) + std::log(std::pow(x, 2) + std::pow(y, 2));
}

int main() {
	/* first function */
	std::vector<std::tuple<double, double>> rDomain1;
	std::vector<std::tuple<uint16_t, uint16_t>> mantissaDomain;
	rDomain1.emplace_back(-10.0, 10.0); // value domains for every variable, add one for each chromosome
	mantissaDomain.emplace_back(0, 23); // set which bits to mutate in every chromosome (we'll modify only the mantissa)

	Population<32> f1Population1(10, rDomain1), f1Population2(10, rDomain1), f1Population3(10, rDomain1); // create the populationn
	f1Population1.SetGenesToMutate(mantissaDomain); // and set the mutation domain
	f1Population2.SetGenesToMutate(mantissaDomain);
	f1Population3.SetGenesToMutate(mantissaDomain);

	GeneticOptimizer<32> optimizer; // create the optimizer and set everything
	optimizer.SetFunctionToOptimize(f1);
	optimizer.SetHowToOptimize(false); // false - minimize, true - maximize
	optimizer.SetPopulation(f1Population1);
	optimizer.Optimize("f1_1.out", 10);
	optimizer.SetPopulation(f1Population2);
	optimizer.Optimize("f1_2.out", 10);
	optimizer.SetPopulation(f1Population3);
	optimizer.Optimize("f1_3.out", 10);

	/* second function */
	std::vector<std::tuple<double, double>> rDomain2;
	rDomain2.emplace_back(-5, 3);
	rDomain2.emplace_back(2, 10);

	Population<32> f2Population1(10, rDomain2), f2Population2(10, rDomain2), f2Population3(10, rDomain2); // create the populationn
	mantissaDomain.emplace_back(0, 23); // adding mutation domain for the second chromosome
	f2Population1.SetGenesToMutate(mantissaDomain); // and set the mutation domain
	f2Population2.SetGenesToMutate(mantissaDomain);
	f2Population3.SetGenesToMutate(mantissaDomain);

	optimizer.SetFunctionToOptimize(f2);
	optimizer.SetHowToOptimize(true); // false - minimize, true - maximize
	optimizer.SetPopulation(f2Population1);
	optimizer.Optimize("f2_1.out", 10);
	optimizer.SetPopulation(f2Population2);
	optimizer.Optimize("f2_2.out", 10);
	optimizer.SetPopulation(f2Population3);
	optimizer.Optimize("f2_3.out", 10);

	return EXIT_SUCCESS;
}