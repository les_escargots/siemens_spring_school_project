#include "Optimizer/Population.h"

template<size_t nGenes>
double const Population<nGenes>::sc_selectionChance = 0.25;

template<size_t nGenes>
inline Population<nGenes>::Population(uint32_t populationSize, uint32_t numberOfChromosomes)
{
	for (uint32_t iIndividual = 0; iIndividual < populationSize; ++iIndividual)
	{
		m_population.emplace_back(numberOfChromosomes);
	}
}

template<size_t nGenes>
inline Population<nGenes>::Population(uint32_t populationSize, const std::vector<std::tuple<int64_t, int64_t>>& integerDomains)
{
	for (uint32_t iIndividual = 0; iIndividual < populationSize; ++iIndividual)
	{
		m_population.emplace_back(integerDomains.size(), integerDomains);
	}
}

template<size_t nGenes>
inline Population<nGenes>::Population(uint32_t populationSize, const std::vector<std::tuple<double, double>>& realDomains)
{
	for (uint32_t iIndividual = 0; iIndividual < populationSize; ++iIndividual)
	{
		m_population.emplace_back(realDomains.size(), realDomains);
	}
}

template<size_t nGenes>
inline auto Population<nGenes>::operator=(const Population& other) -> Population
{
	m_population = other.m_population;
	m_genesToMutate = other.m_genesToMutate;
	return *this;
}

template<size_t nGenes>
inline void Population<nGenes>::SetGenesToMutate(const std::vector<std::tuple<uint16_t, uint16_t>>& genesToMutate)
{
	m_genesToMutate = genesToMutate;
}

template<size_t nGenes>
inline auto Population<nGenes>::GenerateChildren() const -> Population
{
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<> probabilityDist{ 0, 1 };
	std::tuple<uint32_t, uint32_t> selectedParents{ UINT32_MAX, UINT32_MAX };
	std::vector<Individual<nGenes>> childrenPopulation;
	for (uint32_t iIndividual = 0; iIndividual < m_population.size(); ++iIndividual)
	{
		if (probabilityDist(randEngine) <= sc_selectionChance)
		{
			if (std::get<0>(selectedParents) == UINT32_MAX)
			{
				std::get<0>(selectedParents) = iIndividual;
			}
			else
			{
				std::get<1>(selectedParents) = iIndividual;
				auto children = Individual<nGenes>::CreateChildren(std::tie(m_population[std::get<0>(selectedParents)],
					m_population[std::get<1>(selectedParents)]));
				childrenPopulation.push_back(std::get<0>(children));
				childrenPopulation.push_back(std::get<1>(children));
				selectedParents = { UINT32_MAX, UINT32_MAX };
			}
		}
		else
		{
			childrenPopulation.push_back(m_population[iIndividual]);
		}
	}
	if (std::get<0>(selectedParents) != UINT32_MAX)
	{
		childrenPopulation.push_back(m_population[std::get<0>(selectedParents)]);
	}
	Population<nGenes> returnPopulation;
	returnPopulation.m_population = childrenPopulation;
	returnPopulation.m_genesToMutate = m_genesToMutate;
	return returnPopulation;
}

template<size_t nGenes>
inline void Population<nGenes>::Mutate()
{
	for (uint32_t iIndividual = 0; iIndividual < m_population.size(); ++iIndividual)
	{
		if (m_population[iIndividual].GetNumberOfChromosomes() != m_genesToMutate.size())
		{
			throw std::exception("Number of chromosomes and number of mutation domains don't match.");
		}
		m_population[iIndividual].Mutate(m_genesToMutate);
	}
}

template<size_t nGenes>
inline std::ostream & operator<<(std::ostream & os, const Population<nGenes>& population)
{
	for (uint32_t iIndividual = 0; iIndividual < population.m_population.size(); ++iIndividual)
	{
		os << "Individual " << iIndividual << ":\n" << population.m_population[iIndividual];
	}
	return os;
}
