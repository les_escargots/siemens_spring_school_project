#include "Optimizer/Individual.h"

template<size_t nGenes>
double const Individual<nGenes>::sc_mutationChance = 0.05;

template<size_t nGenes>
inline auto Individual<nGenes>::CreateChildren(const Individual& mom, const Individual& dad) -> std::tuple<Individual, Individual>
{
	if (mom.GetNumberOfChromosomes() != dad.GetNumberOfChromosomes())
	{
		throw std::exception("Trying to create children with parents with different chromosomes counts.");
	}
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<> bitsDist{ 0, nGenes };
	Individual child1(mom), child2(dad);
	for (uint32_t iChromosome = 0; iChromosome < mom.GetNumberOfChromosomes(); ++iChromosome)
	{
		uint32_t splicePoint = bitsDist(randEngine);
		for (uint16_t iGenes = splicePoint; iGenes < nGenes; ++iGenes)
		{
			bool temp = child1.m_chromosomes[iChromosome][iGenes];
			child1.m_chromosomes[iChromosome][iGenes] = child2.m_chromosomes[iChromosome][iGenes];
			child2.m_chromosomes[iChromosome][iGenes] = temp;
		}
	}
	return { child1, child2 };
}

template<size_t nGenes>
inline auto Individual<nGenes>::CreateChildren(std::tuple<const Individual&, const Individual&> parents) -> std::tuple<Individual, Individual>
{
	if (std::get<0>(parents).GetNumberOfChromosomes() != std::get<1>(parents).GetNumberOfChromosomes())
	{
		throw std::exception("Trying to create children with parents with different chromosomes counts.");
	}
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<> bitsDist{ 0, nGenes };
	Individual child1(std::get<0>(parents)), child2(std::get<1>(parents));
	for (uint32_t iChromosome = 0; iChromosome < std::get<0>(parents).GetNumberOfChromosomes(); ++iChromosome)
	{
		uint32_t splicePoint = bitsDist(randEngine);
		for (uint16_t iGenes = splicePoint; iGenes < nGenes; ++iGenes)
		{
			bool temp = child1.m_chromosomes[iChromosome][iGenes];
			child1.m_chromosomes[iChromosome][iGenes] = child2.m_chromosomes[iChromosome][iGenes];
			child2.m_chromosomes[iChromosome][iGenes] = temp;
		}
	}
	return { child1, child2 };
}

template<size_t nGenes>
inline Individual<nGenes>::Individual(uint32_t nChromosomes, bool random) :
	m_integerValues(false),
	m_realValues(false)
{
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_int_distribution<> bitDist{ 0, 1 };
	for (uint32_t iChromosomes = 0; iChromosomes < nChromosomes; ++iChromosomes)
	{
		std::bitset<nGenes> chromosome;
		if (random == true)
		{
			for (uint32_t iGenes = 0; iGenes < nGenes; ++iGenes)
			{
				chromosome[iGenes] = bitDist(randEngine);
			}
		}
		m_chromosomes.emplace_back(chromosome);
	}
}

template<size_t nGenes>
inline Individual<nGenes>::Individual(const std::bitset<nGenes>& chromosome) :
	m_integerValues(false),
	m_realValues(false)
{
	m_chromosomes.push_back(chromosome);
}

template<size_t nGenes>
inline Individual<nGenes>::Individual(const std::vector<std::bitset<nGenes>>& chromosomes) :
	m_chromosomes(chromosomes),
	m_integerValues(false),
	m_realValues(false)
{}

template<size_t nGenes>
inline Individual<nGenes>::Individual(uint32_t nChromosomes, const std::vector<std::tuple<double, double>>& realDomains) :
	m_integerValues(false),
	m_realValues(true)
{
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	for (uint32_t iChromosomes = 0; iChromosomes < nChromosomes; ++iChromosomes)
	{
		std::uniform_real_distribution<double> realDomainDist(std::get<0>(realDomains[iChromosomes]), std::get<1>(realDomains[iChromosomes]));
		switch (nGenes)
		{
		case 64:
		{
			double individual = realDomainDist(randEngine);
			uint64_t bits = *reinterpret_cast<uint64_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		case 32:
		{
			float individual = realDomainDist(randEngine);
			uint32_t bits = *reinterpret_cast<uint32_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		default:
		{
			std::string exceptionMessage("Unimplemented gene size for real domain: " + std::to_string(nGenes));
			throw std::exception(exceptionMessage.c_str());
		} break;
		}
	}
}

template<size_t nGenes>
inline Individual<nGenes>::Individual(uint32_t nChromosomes, const std::vector<std::tuple<int64_t, int64_t>>& integerDomains) :
	m_integerValues(true),
	m_realValues(false)
{
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	for (uint32_t iChromosomes = 0; iChromosomes < nChromosomes; ++iChromosomes)
	{
		std::uniform_int_distribution<int64_t> integerDomainDist(std::get<0>(integerDomains[iChromosomes]), std::get<1>(integerDomains[iChromosomes]));
		switch (nGenes)
		{
		case 64:
		{
			int64_t individual = integerDomainDist(randEngine);
			uint64_t bits = *reinterpret_cast<uint64_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		case 32:
		{
			int32_t individual = integerDomainDist(randEngine);
			uint32_t bits = *reinterpret_cast<uint32_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		case 16:
		{
			int16_t individual = integerDomainDist(randEngine);
			uint16_t bits = *reinterpret_cast<uint16_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		case 8:
		{
			int8_t individual = integerDomainDist(randEngine);
			uint8_t bits = *reinterpret_cast<uint8_t*>(&individual);
			m_chromosomes.emplace_back(bits);
		} break;
		default:
		{
			std::string exceptionMessage("Unimplemented gene size for interger domain: " + std::to_string(nGenes));
			throw std::exception(exceptionMessage.c_str());
		} break;
		}
	}
}

template<size_t nGenes>
inline auto Individual<nGenes>::operator=(const Individual& other) -> Individual
{
	m_chromosomes = other.m_chromosomes;
	m_integerValues = other.m_integerValues;
	m_realValues = other.m_realValues;
	return *this;
}

template<size_t nGenes>
inline auto Individual<nGenes>::GetNumberOfChromosomes() const -> uint32_t
{
	return m_chromosomes.size();
}

template<size_t nGenes>
inline auto Individual<nGenes>::GetChromosomes() const -> const std::vector<std::bitset<nGenes>>&
{
	return m_chromosomes;
}

template<size_t nGenes>
inline auto Individual<nGenes>::Mutate(const std::vector<std::tuple<uint16_t, uint16_t>>& genesToMutate) -> Individual<nGenes>
{
	std::default_random_engine randEngine;
	randEngine.seed(std::chrono::system_clock::now().time_since_epoch().count());
	std::uniform_real_distribution<> probabilityDist(0, 1);
	for (uint32_t iChromosomes = 0; iChromosomes < m_chromosomes.size(); ++iChromosomes)
	{
		for (uint16_t iGenes = std::get<0>(genesToMutate[iChromosomes]); iGenes < std::get<1>(genesToMutate[iChromosomes]); ++iGenes)
		{
			if (probabilityDist(randEngine) <= sc_mutationChance)
			{
				m_chromosomes[iChromosomes][iGenes] = !m_chromosomes[iChromosomes][iGenes];
			}
		}
	}
	return *this;
}

template<size_t nGenes>
inline std::ostream & operator<<(std::ostream & os, const Individual<nGenes>& individual)
{
	for (const std::bitset<nGenes>& chromosome : individual.GetChromosomes())
	{
		os << "\t" << chromosome.to_string() << " ";
		if (individual.m_integerValues == true)
		{
			switch (nGenes)
			{
			case 8:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueInt8;
			} break;
			case 16:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueInt16;
			} break;
			case 32:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueInt32;
			} break;
			case 64:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueInt64;
			} break;
			}
		}
		if (individual.m_realValues == true)
		{
			switch (nGenes)
			{
			case 32:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueReal32;
			} break;
			case 64:
			{
				os << BitsToValueConverter<nGenes>(chromosome).valueReal64;
			} break;
			}
		}
		os << "\n";
	}
	return os;
}
