#pragma once
#include <fstream>
#include <array>
#include <string>

class BeamGenerator {
public:
	static uint32_t GetNumberOfNodesPerStoryRow(uint32_t widthSplits);
	static uint32_t GetNumberOfNodesPerStory(uint32_t widthSplits);
	static uint32_t GetNumberOfNodesPerWall(uint32_t widthSplits, uint32_t heightSplits);
	static uint32_t GetNumberOfNodes(uint32_t widthSplits, uint32_t heightSplits);
	static uint32_t GetNumberOfTetrahedrons(uint32_t widthSplits, uint32_t heightSplits);
	static uint32_t GetNumberOfCubesPerStoryRow(uint32_t widthSplits);
	static uint32_t GetNumberOfCubesPerStory(uint32_t widthSplits);
	static uint32_t GetNumberOfCubes(uint32_t widthSplits, uint32_t heightSplits);
	static std::array<uint32_t, 8> GetCubeNodeIndices(uint32_t cubeIndex, uint32_t widthSplits, uint32_t heightSplits);

	static void PrintBeamNodes(std::ofstream& outputFile, double width, double height, uint32_t widthSplits, uint32_t heightSplits);
	static void PrintBeamTetrahedronsIndices(std::ofstream& outputFile, uint32_t widthSplits, uint32_t heightSplits);

	static void GenerateBeam(const std::string& fileName, double width, double height, uint32_t widthSplits, uint32_t heightSplits);

private:
	BeamGenerator() = default;
};