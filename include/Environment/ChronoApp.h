#pragma once
#include <Environment/BeamGenerator.h>
#include <Environment/LoaderStiffLoadNode.h>
#include <chrono/fea/ChContactSurfaceMesh.h>
#include <chrono/fea/ChLinkPointFrame.h>
#include <chrono/fea/ChMeshFileLoader.h>
#include <chrono/fea/ChVisualizationFEAmesh.h>
#include <chrono/physics/ChLoadContainer.h>
#include <chrono_irrlicht/ChIrrApp.h>

#include <chrono/fea/ChLinkPointPoint.h>
#include "chrono/fea/ChBuilderBeam.h"
#include "chrono/fea/ChElementCableANCF.h"
#include "chrono/fea/ChLinkDirFrame.h"
#include "chrono/fea/ChLinkPointFrame.h"
#include "chrono/fea/ChMesh.h"
#include "chrono/fea/ChVisualizationFEAmesh.h"
#include "chrono/physics/ChBodyEasy.h"
#include "chrono/physics/ChSystem.h"
#include "chrono/physics/ChSystemNSC.h"
#include "chrono/solver/ChSolverMINRES.h"
#include "chrono/solver/ChSolverPMINRES.h"
#include "chrono/timestepper/ChTimestepper.h"
#include "chrono_irrlicht/ChIrrApp.h"
#include <chrono/fea/ChElementTetra_4.h>

#include <tuple>
using std::tuple;
using ElementTetra_ptr = std::shared_ptr<ChElementTetra_4>;

using namespace chrono;
using namespace chrono::fea;
using namespace chrono::irrlicht;
using namespace irr;


class ChronoApp {
public:
	static ChronoApp& GetInstance();

private:
	ChronoApp();
	ChronoApp(ChronoApp const&) = delete;
	void operator=(ChronoApp const&) = delete;
	~ChronoApp() = default ;

private:
	void PrepareAppForStart();
	void SetupScene();
	void CreateObjects();
	void RotateBeamMesh(const std::shared_ptr<ChMesh> & beamMesh) const;
	void CreateConstraintsNodesWall(const std::shared_ptr<ChMesh>& beamMesh, const std::shared_ptr<ChBodyEasyBox>& wall);
	void AddLoadToNode(const std::shared_ptr<ChLoadContainer>& loadContainer, const std::shared_ptr<ChNodeFEAxyz>& feaNode) const;
	void BeamMeshSurfaceMaterialConfiguration(const std::shared_ptr<ChMesh>& beamMesh) const;
	void ApplicationSetup();
	void AttachCableWithBox(const std::shared_ptr<ChNodeFEAxyz>& firstNode, const std::shared_ptr<ChNodeFEAxyz>& secondNode);

public:
	void StartMainLoop();

	// Helpers
	static tuple<ElementTetra_ptr, ElementTetra_ptr> GetLeastMostStrainElements(const std::shared_ptr<ChMesh>& beamMesh);
private:
	ChSystemNSC m_system;
	ChIrrApp m_application;
};
