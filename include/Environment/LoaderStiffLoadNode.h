#pragma once
#include <chrono/physics/ChLoaderUVW.h>
#include <chrono/fea/ChNodeFEAxyz.h>

using namespace chrono;
using namespace fea;

class LoaderStiffLoadNode : public ChLoaderUVWatomic {
public:
	LoaderStiffLoadNode(std::shared_ptr<ChLoadableUVW> mloadable);
	virtual void ComputeF(const double U, const double V, const double W, ChVectorDynamic<>& F, ChVectorDynamic<>* state_x, ChVectorDynamic<>* state_w);

	virtual bool IsStiff();
};