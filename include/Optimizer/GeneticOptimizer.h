#pragma once
#include <functional>
#include <fstream>
#include <algorithm>
#include "Population.h"

template<size_t nGenes>
class GeneticOptimizer
{
protected:
	/* The population of the current generation. */
	Population<nGenes> m_population;
	/* The function that we'll be needing to optimize. */
	std::function<double(const std::vector<std::bitset<nGenes>>&)> m_functionToOptimize;
	/* Flag which tells us how to optimize the function: false - minimize it, true - maximize it. */
	bool m_howToOptimize;

	/* Sort the population in ascending order based on the function value. */
	void SortPopulation();
	/* Get a softmax vector which tells us how fit is every individual (with values between 0 and 1, all adding up to 1). */
	auto GetSelectionProbabilities() -> std::vector<double>;
	/* Get points between 0 and 1 which act as selection domains for every individual. As their fitness is higher, so will be
	their domain. */
	auto GetCummulativeProbabilities() -> std::vector<double>;
	/* Go through the population and duplicate them based on their selection domain (fitter individuals will have a chance of
	appearing multiple times). */
	void ReselectPopulation();

public:
	/* Set the initial population to evolve. */
	void SetPopulation(const Population<nGenes>& population);
	/* Set the function to optimize. */
	void SetFunctionToOptimize(std::function<double(const std::vector<std::bitset<nGenes>>&)> functionToOptimize);
	/* Set the optimization flag. */
	void SetHowToOptimize(bool howToOptimize);

	/* Begin the optimization progress. A log will be written to a new file. */
	void Optimize(const std::string& fileName, uint32_t numberOfGenerations, bool verbose = false);
};

#include "../source/Optimizer/GeneticOptimizer.hpp"