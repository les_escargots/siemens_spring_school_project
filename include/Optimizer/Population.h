#pragma once
#include "Individual.h"

template<size_t nGenes> class GeneticOptimizer;

template<size_t nGenes>
class Population
{
protected:
	static double const sc_selectionChance;
	std::vector<Individual<nGenes>> m_population;
	/* The index mutation domain for each cromosome. In order to avoid going out of our domain,
	we'll tell the population which bits it'll be able to safely mutate. */
	std::vector<std::tuple<uint16_t, uint16_t>> m_genesToMutate;

public:
	Population() = default;
	Population(const Population&) = default;
	/* Generate a population at random. */
	Population(uint32_t populationSize, uint32_t numberOfChromosomes);
	/* Generate a population in an integer domain. */
	Population(uint32_t populationSize, const std::vector<std::tuple<int64_t, int64_t>>& integerDomains);
	/* Generate a population in a real domain. */
	Population(uint32_t populationSize, const std::vector<std::tuple<double, double>>& realDomains);
	~Population() = default;

	auto operator=(const Population& other) -> Population;

	/* Set the index mutation domain for each chromosome. */
	void SetGenesToMutate(const std::vector<std::tuple<uint16_t, uint16_t>>& genesToMutate);
	/* Generate a new population through splicing. */
	auto GenerateChildren() const-> Population;
	/* Mutate the population. */
	void Mutate();

	template<size_t nGenes>
	friend std::ostream& operator<<(std::ostream& os, const Population<nGenes>& population);

	friend GeneticOptimizer<nGenes>;
};

#include "../source/Optimizer/Population.hpp"