#pragma once
#include <bitset>
#include <vector>
#include <tuple>
#include <random>
#include <chrono>
#include <string>
#include <ostream>
#include "BitsToValueConverter.h"

template<size_t nGenes> 
class Individual 
{
protected:
	static double const sc_mutationChance;
	std::vector<std::bitset<nGenes>> m_chromosomes;
	/* Flag to know if we hold signed integer values. */
	bool m_integerValues;
	/* Flag to know if we hold real values. */
	bool m_realValues;

public:
	/* Create two children from splicing two parents. */
	static auto CreateChildren(const Individual& mom, const Individual& dad) -> std::tuple<Individual, Individual>;
	static auto CreateChildren(std::tuple<const Individual&, const Individual&> parents)->std::tuple<Individual, Individual>;

	Individual() = default;
	Individual(const Individual&) = default;
	/* Create an individual that has n random chromosomes by default. Turn bool to false to disable random initialization. */
	Individual(uint32_t nChromosomes, bool random = true);
	/* Create an individual that has 1 chromosome from an existing one. */
	Individual(const std::bitset<nGenes>& chromosome);
	/* Create an individual that has n chromosomes from existing ones. */
	Individual(const std::vector<std::bitset<nGenes>>& chromosomes);
	/* The next two functions throw exceptions for non-standard bit sizes. */
	/* Create an individual that has n chromosomes with each chromosome bound to a real interval. */
	Individual(uint32_t nChromosomes, const std::vector<std::tuple<double, double>>& realDomains);
	/* Create an individual that has n chromosomes with each chromosome bound to a signed integer interval. */
	Individual(uint32_t nChromosomes, const std::vector<std::tuple<int64_t, int64_t>>& integerDomains);
	~Individual() = default;

	auto operator=(const Individual& other) -> Individual;

	/* Returns the number of chromosomes */
	auto GetNumberOfChromosomes() const -> uint32_t;
	/* Return the chromosomes. */
	auto GetChromosomes() const -> const std::vector<std::bitset<nGenes>>&;

	/* Mutate the genes of a chromosome in certain positions (to limit jumping out of domains). Keep in mind
	that index 0 holds the least significant bit. */
	auto Mutate(const std::vector<std::tuple<uint16_t, uint16_t>>& genesToMutate) -> Individual;

	template<size_t nGenes>
	friend std::ostream& operator<<(std::ostream& os, const Individual<nGenes>& individual);
};

#include "../source/Optimizer/Individual.hpp"