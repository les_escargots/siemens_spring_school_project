#pragma once
#include <stdint.h>
#include <bitset>

/* Union used to convert bitsets to values. Can be extended for other values other than basic types.
Just add a special case for your bitset size. */
template<size_t nGenes>
union BitsToValueConverter
{
public:
	/* Value fields. Select from here whichever value you're after. */
	int8_t  valueInt8;
	int16_t valueInt16;
	int32_t valueInt32;
	int64_t valueInt64;
	float   valueReal32;
	double  valueReal64;

	/* Bits field. We only need a 64 bit size as it can fit all primitives. For bigger bitsets you'll need something different. */
	uint64_t bits;

	/* We'll convert a single bitset (can't hold all the chromosomes of an individual). */
	BitsToValueConverter(const std::bitset<nGenes>& bitset);
};

template<size_t nGenes>
inline BitsToValueConverter<nGenes>::BitsToValueConverter(const std::bitset<nGenes>& bitset) :
	bits(bitset.to_ullong())
{}
